export const heightMobileUI = 896;
export const widthMobileUI = 414;

export const API_TIMEOUT = 120000;
export const API_KEY = 'b6369324e0e5340c04a0fcfb4a361d79';
export const API_RAJAONGKIR = 'https://api.rajaongkir.com/starter/';
export const API_HEADER_RAJAONGKIR = {
  key: API_KEY,
};
