import {FotoProfile} from '../../assets';

export const dummyProfile = {
  nama: 'Royan Husen Fatih',
  email: 'royanhusen77@gmail.com',
  nomerHp: '0898881955',
  alamat: 'Jl.Daan Moogot',
  kota: 'Tangerang',
  provinsi: 'Banten',
  avatar: FotoProfile,
};
