import {Wanita, Dewasa, Anak, Bayi} from '../../assets';

export const dummyButtonHeader = [
  {
    id: 1,
    nama: 'Wanita',
    gambar: Wanita,
  },
  {
    id: 2,
    nama: 'Dewasa',
    gambar: Dewasa,
  },
  {
    id: 3,
    nama: 'Anak',
    gambar: Anak,
  },
  {
    id: 4,
    nama: 'Bayi',
    gambar: Bayi,
  },
];
