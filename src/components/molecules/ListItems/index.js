import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {CardItem} from '../../atoms';

const ListItems = ({itemss, navigation}) => {
  return (
    <View style={styles.container}>
      {itemss.map(item => {
        return <CardItem key={item.id} item={item} navigation={navigation} />;
      })}
    </View>
  );
};

export default ListItems;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginTop: 10,
  },
});
