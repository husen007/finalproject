import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {CardKatergori} from '../../atoms';

const ListButtonHeader = ({katergoriss}) => {
  return (
    <View style={styles.container}>
      {katergoriss.map(katergori => {
        return <CardKatergori katergori={katergori} key={katergori.id} />;
      })}
    </View>
  );
};

export default ListButtonHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
});
