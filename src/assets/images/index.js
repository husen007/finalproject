import Slider1 from './Slider1.png';
import Slider2 from './Slider2.png';
import Wanita from './Wanita.png';
import Dewasa from './Dewasa.png';
import Anak from './Anak.png';
import Bayi from './Bayi.png';
import Logo from './logo.svg';
import IlustrasiRegister1 from './register1';
import IlustrasiRegister2 from './register2';

export {
  Slider1,
  Slider2,
  IlustrasiRegister1,
  IlustrasiRegister2,
  Logo,
  Wanita,
  Dewasa,
  Anak,
  Bayi,
};
