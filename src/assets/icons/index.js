import IconLogo from './Logo.svg';
import IconHomeAktif from './home-aktif.svg';
import IconHome from './home.svg';
import IconPakaianAktif from './pakaian-aktif.svg';
import IconPakaian from './pakaian.svg';
import IconProfileAktif from './profile-aktif.svg';
import IconProfile from './profile.svg';
import IconCari from './search.svg';
import IconKeranjang from './shopping-cart.svg';
import IconArrowRight from './arrow-right.svg';
import IconChangePassword from './change-password.svg';
import IconEditProfile from './user-edit.svg';
import IconSignOut from './sign-out.svg';
import IconHistory from './history.svg';
import IconBack from './arrow-left.svg';
import IconKeranjangPutih from './keranjang-putih.svg';
import IconHapus from './hapus.svg';
import IconSubmit from './submit.svg';
import FotoProfile from './Profile-name.png';

export {
  FotoProfile,
  IconLogo,
  IconHome,
  IconHomeAktif,
  IconPakaianAktif,
  IconPakaian,
  IconProfileAktif,
  IconProfile,
  IconCari,
  IconKeranjang,
  IconArrowRight,
  IconChangePassword,
  IconEditProfile,
  IconHistory,
  IconSignOut,
  IconBack,
  IconKeranjangPutih,
  IconHapus,
  IconSubmit,
};
