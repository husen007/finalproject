import WanitaDepanSatu from './wanita1.png';
import WanitaBelakangDua from './wanita2.png';
import WanitaDepanTiga from './wanita3.png';
import WanitaDepanEmpat from './wanita4.png';

import DewasaDepanSatu from './dewasa1.png';
import DewasaBelakangDua from './dewasa2.png';
import DewasaDepanTiga from './dewasa3.png';
import DewasaDepanEmpat from './dewasa4.png';

import AnakDepanSatu from './anak1.png';
import AnakBelakangDua from './anak2.png';
import AnakDepanTiga from './anak3.png';
import AnakDepanEmpat from './anak4.png';

import BayiDepanSatu from './bayi1.png';
import BayiBelakangDua from './bayi2.png';
import BayiDepanTiga from './bayi3.png';
import BayiDepanEmpat from './wanita4.png';

export {
  WanitaDepanSatu,
  WanitaBelakangDua,
  WanitaDepanTiga,
  WanitaDepanEmpat,
  DewasaDepanSatu,
  DewasaBelakangDua,
  DewasaDepanTiga,
  DewasaDepanEmpat,
  AnakDepanSatu,
  AnakBelakangDua,
  AnakDepanTiga,
  AnakDepanEmpat,
  BayiDepanSatu,
  BayiBelakangDua,
  BayiDepanTiga,
  BayiDepanEmpat,
};
