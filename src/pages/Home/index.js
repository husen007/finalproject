import React, {Component} from 'react';
import {Text, StyleSheet, View, ScrollView} from 'react-native';
import {
  BannerSlider,
  HeaderComponent,
  ListButtonHeader,
  ListItems,
} from '../../components';
import {colors, fonts} from '../../utils';
import {dummyButtonHeader, dummyItem} from '../../data';
import {Jarak, Tombol} from '../../components';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      katergoriss: dummyButtonHeader,
      itemss: dummyItem,
    };
  }

  render() {
    const {katergoriss, itemss} = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.page}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <HeaderComponent navigation={navigation} />
          <BannerSlider />
          <View style={styles.pilihKatergori}>
            <Text style={styles.label}>Pilih Kategori</Text>
            <ListButtonHeader katergoriss={katergoriss} />
          </View>

          <View style={styles.pilihItems}>
            <Text style={styles.label}>
              Pilih <Text style={styles.boldLabel}>Pakaian</Text> yang anda
              inginkan
            </Text>
            <ListItems itemss={itemss} navigation={navigation} />
            <Tombol title="Lihat Semua" type="text" padding={10} />
          </View>
        </ScrollView>
        <Jarak height={100} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white},
  pilihKatergori: {
    marginHorizontal: 30,
    marginTop: 10,
  },
  pilihItems: {
    marginHorizontal: 30,
    marginTop: 10,
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.primary.regular,
  },
  boldLabel: {
    fontSize: 18,
    // fontFamily: fonts.primary.bold,
    fontFamily: 'bold',
  },
});
