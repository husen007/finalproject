import Home from './Home';
import Splash from './Splash';
import ListItem from './ListItem';
import Profile from './Profile';
import ProdukDetail from './ProdukDetail';
import Keranjang from './Keranjang';
import Checkout from './Checkout';
import EditProfile from './EditProfile';
import ChangePassword from './ChangePassword';
import History from './History';
import Login from './Login';
import Register1 from './Register/Register1';
import Register2 from './Register/Register2';

export {
  Home,
  Splash,
  ListItem,
  Profile,
  EditProfile,
  Keranjang,
  Checkout,
  ProdukDetail,
  ChangePassword,
  History,
  Login,
  Register1,
  Register2,
};
