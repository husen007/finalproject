import React, {useState} from 'react';
import {Alert, StyleSheet, Text, View} from 'react-native';
import {Logo} from '../../assets';
import {Inputan, Jarak, Tombol} from '../../components';
import {colors, fonts, responsiveHeight} from '../../utils';

export default function Login({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const login = () => {
    if (email && password) {
      navigation.navigate('Register1');
    } else {
      Alert.alert('Error', 'Email & Password harus diisi');
    }
  };

  return (
    <View style={styles.pages}>
      <View style={styles.logo}>
        <Logo />
      </View>
      <View style={styles.cardLogin}>
        <Inputan
          label="Email"
          value={email}
          onChangeText={email => setEmail(email)}
        />
        <Inputan
          label="Password"
          secureTextEntry
          value={password}
          onChangeText={password => setPassword(password)}
        />
        <Jarak height={25} />
        <Tombol
          title="Login"
          type="text"
          padding={12}
          fontSize={18}
          onPress={() => login()}
        />
      </View>

      <View style={styles.register}>
        <Text style={styles.textLabel}>Belum Punya Akun ?</Text>
        <Text
          style={styles.textRegister}
          onPress={() => navigation.navigate('Register1')}>
          Klik Untuk Daftar
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: colors.white,
  },
  ilustrasi: {
    position: 'absolute',
    bottom: 0,
    right: -100,
  },
  logo: {
    alignItems: 'center',
    marginTop: responsiveHeight(70),
  },
  cardLogin: {
    backgroundColor: colors.white,
    marginHorizontal: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    padding: 30,
    borderRadius: 10,
    marginTop: 10,
  },
  register: {
    alignItems: 'center',
    marginTop: 10,
  },
  textRegister: {
    fontSize: 18,
    color: colors.primary,
  },
  textLabel: {
    fontSize: 18,
  },
});
