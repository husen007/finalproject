import firebase from 'firebase';

firebase.initializeApp({
  apiKey: 'AIzaSyCJ1z7PzIMf2ipiwmYkb8PAUT2EKVyOfxw',
  authDomain: 'finalprojectsanbercode-b1f2e.firebaseapp.com',
  projectId: 'finalprojectsanbercode-b1f2e',
  storageBucket: 'finalprojectsanbercode-b1f2e.appspot.com',
  messagingSenderId: '455975582794',
  appId: '1:455975582794:web:a43e4011476a44c2a6fab3',
});

const FIREBASE = firebase;

export default FIREBASE;
